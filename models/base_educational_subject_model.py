from odoo import models, fields


class SubjectModel(models.Model):
    _name = "subject.subject"
    _description = "Subject"

    name = fields.Char(string="Name subject")
