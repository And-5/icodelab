from odoo import models, fields


class ClassSchoolModel(models.Model):
    _name = "class.school"
    _description = "Class school"

    name = fields.Char(string="Name class")
