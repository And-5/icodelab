from odoo import models, fields


class PhoneNumberModel(models.Model):
    _name = "phone.number"
    _description = "Phone number"

    number = fields.Char(string="Phone number")
    user_id = fields.Many2one(comodel_name="base.education", string="User")

    _rec_name = "number"
