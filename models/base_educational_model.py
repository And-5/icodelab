from odoo import models, fields, api, _
from odoo.exceptions import UserError


class BaseEducationModel(models.Model):
    _name = "base.education"
    _description = "Base education"
    _rec_name = "first_name"

    first_name = fields.Char(string="First name")
    last_name = fields.Char(string="Last name")
    father_name = fields.Char(string="Father name")
    age = fields.Integer(string="Age")
    weight = fields.Float(string="Weight")
    note = fields.Text(string="Note")
    isChild = fields.Boolean(string="Child")
    file = fields.Binary(string="File")
    image = fields.Image(string="Image")
    html = fields.Html(string="Html")
    gender = fields.Selection(selection=[
        ("male", "Male"),
        ("female", "Female"),
    ], string="Gender")
    date_of_birthday = fields.Date(string="Date of birthday")
    time_of_birthday = fields.Datetime(string="Time of birthday")
    date_start = fields.Datetime(
        string="Start",
        default=fields.datetime.today(),
    )
    # money = fields.Monetary(string="Money")
    class_at_school = fields.Many2one(
        comodel_name="class.school",
        string="Class"
    )
    subject = fields.Many2many(
        comodel_name="subject.subject",
        string="Subject"
    )
    phone_number = fields.One2many(
        comodel_name="phone.number",
        inverse_name="user_id",
        ondelete="cascade",
        string="Phone number"
    )
    filled_field = fields.Text(string="Filled field")
    similar_base_education_ids = fields.One2many(
        comodel_name="base.education",
        inverse_name="related_base_education_id",
        string="Similar base education"
    )
    related_base_education_id = fields.Many2one(
        comodel_name="base.education",
        string="Related Base Education"
    )
    similar_base_education_count = fields.Integer(
        string="Count of similar base education models",
        compute="_compute_similar_base_education_count",
        store=True
    )
    total_hour_per_task = fields.Float(string="Total hour per task")
    time_spent = fields.Float(string="Time spent")

    def filled_field_refresh(self):
        all_fields = (
                set(self._fields)
                - set(models.MAGIC_COLUMNS)
                - {"display_name", models.Model.CONCURRENCY_CHECK_FIELD}
        )
        filled_fields = tuple(field for field in all_fields if getattr(self, field))
        self.filled_field = "\n".join(filled_fields)

    @api.constrains("total_hour_per_task", "time_spent")
    def _check_filled_in_field(self):
        for record in self:
            if record.total_hour_per_task == 0.0 or record.time_spent == 0.0:
                raise UserError(_("Field total_hour_per_task or time_spent can not be = 0"))

    @api.onchange("time_spent")
    def _onchange_time_spent(self):
        if self.total_hour_per_task != 0:
            self.total_hour_per_task -= self.time_spent

    def show_similar_educations(self):
        return {
            "name": _("Similar Base Educations"),
            "type": "ir.actions.act_window",
            "res_model": "base.education",
            "view_mode": "tree",
            "domain": [("related_base_education_id", "=", self.id)],
        }

    def action_show_not_added_record(self):
        return {
            "name": _("Show Without Similar"),
            "type": "ir.actions.act_window",
            "res_model": "base.education.wizard",
            "view_mode": "form",
            "target": "new",
        }

    @api.depends("similar_base_education_ids")
    def _compute_similar_base_education_count(self):
        for record in self:
            count_similar = record.search_count(
                [("related_base_education_id", "=", record.id)]
            )
            record.update(
                {
                    "similar_base_education_count": count_similar,
                }
            )
