from odoo import fields, models


class BaseEducationWizard(models.TransientModel):
    _name = "base.education.wizard"
    _description = "Base Education Wizard"

    base_education_ids = fields.Many2many(
        comodel_name="base.education",
        relation="base_education_rel",
        column1="base_education_wizard_id",
        column2="base_education_id",
        domain=[("related_base_education_id", "=", False)],
        string="Similar base education"
    )

    def action_confirm(self):
        active_id = self.env.context["active_id"]
        for record in self.base_education_ids:
            record.related_base_education_id = active_id
        return {"type": "ir.actions.act_window_close"}

