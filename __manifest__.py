{
    "name": "Base educational module",
    "summary": "Base educational module",
    "sequence": 1,
    "description": """""",
    "depends": ["base"],
    "data": [
        "security/ir.model.access.csv",
        "wizard/base_education_wizard_view.xml",
        "views/base_education_views.xml"
    ],
    "installable": True,
    "assets": {}
}
